import os
import requests
import json
from datetime import datetime, timedelta
from random import randint

# Get (or set if missiong) environment variables
site=os.environ.get('site')
if site==None:
    os.environ['site']=str(input("site: "))
    site=os.environ['site']

mentorsList="MediaWiki:GrowthMentors.json"

userTalkNsName=os.environ.get('userTalkNsName')
if userTalkNsName==None:
    os.environ['userTalkNsName']=str(input("userTalkNsName: "))
    userTalkNsName=os.environ['userTalkNsName']

userNsName=os.environ.get('userNsName')
if userNsName==None:
    os.environ['userNsName']=str(input("userNsName: "))
    userNsName=os.environ['userNsName']

loginUsername=os.environ.get('loginUsername')
if loginUsername==None:
    os.environ['loginUsername']=str(input("loginUsername: "))
    loginUsername=os.environ['loginUsername']

loginPassword=os.environ.get('loginPassword')
if loginPassword==None:
    os.environ['loginPassword']=str(input("loginPassword: "))
    loginPassword=os.environ['loginPassword']

userAgent=os.environ.get('userAgent')
if userAgent==None:
    os.environ['userAgent']=str(input("userAgent: "))
    userAgent=os.environ['userAgent']

# Script variables
summarytxt="Bot: assegnazione automatica della richiesta ad un altro tutor attivo"
appendtxt="\n:''Messaggio automatico'': questa richiesta non è ancora stata evasa e {{subst:ROOTPAGENAME}} risulta essere inattiv{{subst:GENDER:{{ROOTPAGENAME}}|o|a|o/a}} da almeno due giorni. Viene assegnata automaticamente a {{at|$1}}--~~~~" #$1 = new assigned mentor
waitDays=2 #Number of days to wait before reassigning the question; INFLUENCES inactiveMentors.py
inactivityDaysMentorsSelection=0 #Max N of inactivity days a mentor can have to be selected
runsOnMidnight=1 #1 if true, 0 if false; increase by 1 inactivity days

# Non-configurable variables
inactivityDaysMentorsIgnored=waitDays #Max N of inactivity days a mentor can have to be ignored if they haven't answered
date=[datetime.today()-timedelta(waitDays+2), datetime.today()-timedelta(waitDays)] #Array with initial and final date to check

# Setup API requests
S = requests.Session()
URL = "https://"+site+".wikipedia.org/w/api.php"
headers = {'User-Agent': userAgent}


#Check if a mentor has been active in the past inactivityDays days
def isActive(mentor, inactivityDays):
    PARAMS={
        "action": "query",
        "format": "json",
        "list": "usercontribs",
        "ucend": datetime.strftime(datetime.today()-timedelta(inactivityDays), '%Y-%m-%d')+"T00:00:00.000Z",
        "ucuser": mentor
    }
    edits=S.get(url=URL, params=PARAMS, headers=headers).json()['query']['usercontribs']

    PARAMS={
        "action": "query",
        "format": "json",
        "list": "logevents",
        "leend": datetime.strftime(datetime.today()-timedelta(inactivityDays), '%Y-%m-%d')+"T00:00:00.000Z",
        "leuser": mentor,
        "lelimit": "1"
    }
    hasLogs=len(S.get(url=URL, params=PARAMS, headers=headers).json()['query']['logevents']) #returns 0 or 1

    return len(edits)>0 or hasLogs == 1

#Get mentors list
mentors=[]
PARAMS = {
	"action": "parse",
	"format": "json",
	"page": mentorsList,
	"prop": "wikitext",
	"formatversion": "2"
}
R=S.get(url=URL, params=PARAMS, headers=headers).json()['parse']['wikitext']
mentors=[]

#Get a list of all the NUMBERS contained between "\"" and "\"" in the wikitext
parsedMentorsIds=R.split("\"")
parsedMentorsIds=parsedMentorsIds[1::2]

#If is not a number, remove from R
for i in range(len(parsedMentorsIds)-1, -1, -1):
    try:
        parsedMentorsIds[i]=int(parsedMentorsIds[i])
    except:
        parsedMentorsIds.pop(i)

for id in parsedMentorsIds:
    #API call to get mentor's usernamee
    PARAMS = {
        "action": "query",
        "format": "json",
        "list": "users",
        "formatversion": "2",
        "ususerids": id
    }
    mentor=S.get(url=URL, params=PARAMS, headers=headers).json()['query']['users'][0]['name']
    if isActive(mentor, inactivityDaysMentorsSelection+runsOnMidnight):
        mentors+=[mentor]


#Get questions list
PARAMS={
	"action": "query",
	"format": "json",
	"list": "recentchanges",
	"continue": "-||",
	"formatversion": "2",
	"rcstart": datetime.strftime(date[0], '%Y-%m-%d')+"T00:00:00.000Z",
	"rcend": datetime.strftime(date[1], '%Y-%m-%d')+"T00:00:00.000Z",
	"rcdir": "newer",
	"rctag": "mentorship module question",
	"rcprop": "title|timestamp|ids|user|tags",
	"rclimit": "500",
	"rcgeneraterevisions": 1,
	"rcslot": "main"
}
R=S.get(url=URL, params=PARAMS, headers=headers)
listQuestions=R.json()['query']['recentchanges']

#Check if the question has an answer
for question in listQuestions:
    PARAMS={
        "action": "parse",
        "format": "json",
        "oldid": question['revid'],
        "prop": "sections",
        "formatversion": "2"
    }
    R=S.get(url=URL, params=PARAMS, headers=headers)
    section=len(R.json()['parse']['sections'])

    PARAMS={
        "action": "parse",
        "format": "json",
        "oldid": question['revid'],
        "prop": "wikitext",
        "section": section,
        "utf8": 1,
        "ascii": 1,
        "formatversion": "2"
    }
    R=S.get(url=URL, params=PARAMS, headers=headers)

    oldtext=R.json()['parse']['wikitext']
    mentor=question['title'].replace(userTalkNsName, "")
    mentee=question['user']

    PARAMS={
	"action": "parse",
	"format": "json",
	"page": userTalkNsName+mentor,
	"prop": "sections|wikitext",
	"formatversion": "2"
    }
    R=S.get(url=URL, params=PARAMS, headers=headers)
    cursection=None
    for i in range(len(R.json()['parse']['sections']), 1, -1):
        if mentee in R.json()['parse']['sections'][i-1]['line']:
            cursection=i
            break
    if cursection==None:
        #Question undid or archived
        continue

    PARAMS={
        "action": "parse",
        "format": "json",
        "prop": "wikitext",
        "page": userTalkNsName+mentor,
        "section": cursection,
        "utf8": 1,
        "ascii": 1,
        "formatversion": "2"
    }
    R=S.get(url=URL, params=PARAMS, headers=headers)
    curtext=R.json()['parse']['wikitext']

    print("\n\n"+mentee+" => "+mentor) #log

    if curtext != oldtext:
        continue

    PARAMS={
        "action": "query",
        "format": "json",
        "prop": "revisions",
        "titles": userTalkNsName+mentee,
        "formatversion": "2",
        "rvlimit": "1",
        "rvslots": "*",
        "rvend": datetime.strftime(date[0]-timedelta(1), '%Y-%m-%d')+"T00:00:00.000Z",
        "rvuser": mentor
    }
    R=S.get(url=URL, params=PARAMS, headers=headers)
    try:
        #The mentor edited mentee's talk page from at least the initial date in range
        R.json()['query']['pages'][0]['revisions']
        continue
    except KeyError:
        pass

    print("THE QUESTION HASN'T BEEN ANSWERED JET") #log

    if isActive(mentor, inactivityDaysMentorsIgnored+runsOnMidnight):
        print("However, the mentor is currently active") #log
    else:
        #Assign the question to another mentor
        PARAMS_0 = {
            "action": "query",
            "meta": "tokens",
            "type": "login",
            "format": "json"
        }
        R = S.get(url=URL, params=PARAMS_0)
        DATA = R.json()

        LOGIN_TOKEN = DATA['query']['tokens']['logintoken']

        PARAMS_1 = {
            "action": "login",
            "lgname": loginUsername,
            "lgpassword": loginPassword,
            "lgtoken": LOGIN_TOKEN,
            "format": "json"
        }

        R = S.post(URL, data=PARAMS_1, headers=headers)

        PARAMS_2 = {
            "action": "query",
            "meta": "tokens",
            "format": "json"
        }

        R = S.get(url=URL, params=PARAMS_2, headers=headers)
        DATA = R.json()

        CSRF_TOKEN = DATA['query']['tokens']['csrftoken']

        PARAMS_3 = {
            "action": "edit",
            "title": userTalkNsName+mentor,
            "section": cursection,
            "token": CSRF_TOKEN,
            "summary": summarytxt,
            "format": "json",
            "appendtext": appendtxt.replace("$1", mentors[randint(0,len(mentors)-1)])
        }

        R = S.post(URL, data=PARAMS_3, headers=headers)
        DATA = R.json()
