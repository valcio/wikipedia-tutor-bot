# Wikipedia BOTutor
Wikipedia BOTutor is a Wikipedia bot to easily manage mentorship on the enabled wikis.

## Routines
* **inavctiveMentors.py**: checks that all mentors made any edit or log action in the selected range of past days. If not, writes a warning in the selected notice board page.
* **unansweredQuestions.py**: checks that all the recent questions sent to mentors trough the Newcomer's Homepage have been answered. If not, and the mentors appears inactive from the time of that question, reassigns the question to another mentor by mentioning them in a reply.
* **unansweredQuestions.py**: checks that all the recent requests in a selected discussion page have recieved a reply. If not assigns the question to another mentor by mentioning them in a reply.
* **reassignMentees.py**: reassigns all the mentees of the selected mentor to a new random mentor.

## Contributing
### Installation
1. Clone this git repository
```
git clone https://gitlab.wikimedia.org/valejappo/wikipedia-tutor-bot
```
2. Move into the base directory
```
cd wikipedia-tutor-bot
```

### Requirements
* Python 3^