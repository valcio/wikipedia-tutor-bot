import os
import requests
from datetime import datetime, timedelta

# Get (or set if missiong) environment variables
site=os.environ.get('site')
if site==None:
    os.environ['site']=str(input("site: "))
    site=os.environ['site']

mentorsList="MediaWiki:GrowthMentors.json"

requestsPage=os.environ.get('requestsPage')
if requestsPage==None:
    os.environ['requestsPage']=str(input("requestsPage: "))
    requestsPage=os.environ['requestsPage']

userTalkNsName=os.environ.get('userTalkNsName')
if userTalkNsName==None:
    os.environ['userTalkNsName']=str(input("userTalkNsName: "))
    userTalkNsName=os.environ['userTalkNsName']

userNsName=os.environ.get('userNsName')
if userNsName==None:
    os.environ['userNsName']=str(input("userNsName: "))
    userNsName=os.environ['userNsName']

loginUsername=os.environ.get('loginUsername')
if loginUsername==None:
    os.environ['loginUsername']=str(input("loginUsername: "))
    loginUsername=os.environ['loginUsername']

loginPassword=os.environ.get('loginPassword')
if loginPassword==None:
    os.environ['loginPassword']=str(input("loginPassword: "))
    loginPassword=os.environ['loginPassword']

userAgent=os.environ.get('userAgent')
if userAgent==None:
    os.environ['userAgent']=str(input("userAgent: "))
    userAgent=os.environ['userAgent']

# Setup API requests
S = requests.Session()
URL = "https://"+site+".wikipedia.org/w/api.php"
headers = {'User-Agent': userAgent}

# Script variables
inactivityDays=7
runsOnMidnight=1 #1 if true, 0 if false; increase by 1 inactivity days
summarytxt_mentor="Bot: Avviso tutor della sua inattività"
sectiontitletxt_mentor="Inattività tutor"
txt_mentor="Gentile {{subst:ROOTPAGENAME}},\nho notato che nonostante la tua inattività stai continuando a ricevere domande dai tuoi allievi.\n\nTi informo che puoi impostare il tuo stato di tutor come \"Occupato\" dal [[Special:MentorDashboard|pannello di tutoraggio]].\n\nNel frattempo, il tuo nome potrebbe già essere stato rimosso dalla [["+mentorsList+"|lista dei tutor]]; considera di reinserircelo, possibilmente modificando il tuo stato quando necessario.\n\nSaluti!\n''Messaggio automatico''--~~~~"
summarytxt_mentors="Bot: Avviso inattività del tutor $1" #$1=mentor's username
sectiontitletxt_mentors="Avviso inattività $1" #$1=mentor's username
txt_mentors="Gentili tutor,\nsegnalo l'inattività del tutor {{Utente|$1}}. \n\nNon sembra che abbia impostato il suo stato come inattiv{{GENDER:$1|o|a|o/a}} e potrebbe essere necessaria la rimozione del suo nome dalla [["+mentorsList+"|lista dei tutor]] (anche attraverso [[Speciale:ManageMentors]]).\nGrazie, buon lavoro!\n\n''Messaggio automatico''--~~~~" #$1=mentor's username
waitDays=2 #influenced by unansweredQuestions.py

#Get mentors list
mentors=[]
PARAMS = {
	"action": "parse",
	"format": "json",
	"page": mentorsList,
	"prop": "wikitext",
	"formatversion": "2"
}
R=S.get(url=URL, params=PARAMS, headers=headers).json()['parse']['wikitext']
mentors=[]

#Get a list of all the NUMBERS contained between "\"" and "\"" in the wikitext
parsedMentorsIds=R.split("\"")
parsedMentorsIds=parsedMentorsIds[1::2]

#If is not a number, remove from R
for i in range(len(parsedMentorsIds)-1, -1, -1):
    try:
        parsedMentorsIds[i]=int(parsedMentorsIds[i])
    except:
        parsedMentorsIds.pop(i)

for id in parsedMentorsIds:
    #API call to get mentor's usernamee
    PARAMS = {
        "action": "query",
        "format": "json",
        "list": "users",
        "formatversion": "2",
        "ususerids": id
    }
    mentor=S.get(url=URL, params=PARAMS, headers=headers).json()['query']['users'][0]['name']

    #Check if a mentor has been active in the past inactivityDays days 
    PARAMS={
        "action": "query",
        "format": "json",
        "list": "usercontribs",
        "ucend": datetime.strftime(datetime.today()-timedelta(inactivityDays+runsOnMidnight), '%Y-%m-%d')+"T00:00:00.000Z",
        "ucuser": mentor
    }
    edits=S.get(url=URL, params=PARAMS, headers=headers).json()['query']['usercontribs']

    PARAMS={
        "action": "query",
        "format": "json",
        "list": "logevents",
        "leend": datetime.strftime(datetime.today()-timedelta(inactivityDays+runsOnMidnight), '%Y-%m-%d')+"T00:00:00.000Z",
        "leuser": mentor,
        "lelimit": "1"
    }
    hasLogs=len(S.get(url=URL, params=PARAMS, headers=headers).json()['query']['logevents']) #returns 0 or 1

    isActive=len(edits)>0 or hasLogs == 1

    print(mentor+": "+str(isActive)) #log

    if not isActive:
        #Get questions list
        PARAMS={
            "action": "query",
            "format": "json",
            "list": "recentchanges",
            "continue": "-||",
            "formatversion": "2",
            "rcend": datetime.strftime(datetime.today()-timedelta(inactivityDays+runsOnMidnight), '%Y-%m-%d')+"T00:00:00.000Z",
	    "rcstart": datetime.strftime(datetime.today()-timedelta(waitDays+runsOnMidnight), '%Y-%m-%d')+"T00:00:00.000Z",
            "rcdir": "newer",
            "rctag": "mentorship module question",
            "rclimit": "1",
            "rcgeneraterevisions": 1,
            "rcslot": "main",
            "rctitle": userTalkNsName+mentor
        }
        R=S.get(url=URL, params=PARAMS, headers=headers)
        hadQuestions=len(R.json()['query']['recentchanges']) > 0

        if hadQuestions: # Hadn't set itself as away
            print("IS RECIEVING NEW QUESTIONS") #log
            PARAMS_0 = {
                "action": "query",
                "meta": "tokens",
                "type": "login",
                "format": "json"
            }
            R = S.get(url=URL, params=PARAMS_0, headers=headers)
            DATA = R.json()

            LOGIN_TOKEN = DATA['query']['tokens']['logintoken']

            PARAMS_1 = {
                "action": "login",
                "lgname": loginUsername,
                "lgpassword": loginPassword,
                "lgtoken": LOGIN_TOKEN,
                "format": "json"
            }
            R = S.post(URL, data=PARAMS_1, headers=headers)

            #Write a warning to the mentor
            PARAMS_2 = {
                "action": "query",
                "meta": "tokens",
                "format": "json"
            }

            R = S.get(url=URL, params=PARAMS_2, headers=headers)
            DATA = R.json()

            CSRF_TOKEN = DATA['query']['tokens']['csrftoken']

            PARAMS_3 = {
                "action": "edit",
                "title": userTalkNsName+mentor,
                "section": "new",
                "token": CSRF_TOKEN,
                "summary": summarytxt_mentor,
                "sectiontitle": sectiontitletxt_mentor,
                "format": "json",
                "appendtext": txt_mentor
            }

            R = S.post(URL, data=PARAMS_3, headers=headers)
            DATA = R.json()

            #Write a warning to the other mentors
            PARAMS_2 = {
                "action": "query",
                "meta": "tokens",
                "format": "json"
            }

            R = S.get(url=URL, params=PARAMS_2, headers=headers)
            DATA = R.json()

            CSRF_TOKEN = DATA['query']['tokens']['csrftoken']

            PARAMS_3 = {
                "action": "edit",
                "title": requestsPage,
                "section": "new",
                "token": CSRF_TOKEN,
                "summary": summarytxt_mentors.replace("$1", mentor),
                "sectiontitle": sectiontitletxt_mentors.replace("$1", mentor),
                "format": "json",
                "appendtext": txt_mentors.replace("$1", mentor)
            }


            R = S.post(URL, data=PARAMS_3, headers=headers)
            DATA = R.json()
