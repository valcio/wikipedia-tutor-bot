import os
import sys
import requests
from datetime import datetime, timedelta
from random import randint
import time

# Get (or set if missiong) environment variables
site=os.environ.get('site')
if site==None:
    os.environ['site']=str(input("site: "))
    site=os.environ['site']

mentorsList="MediaWiki:GrowthMentors.json"

userNsName=os.environ.get('userNsName')
if userNsName==None:
    os.environ['userNsName']=str(input("userNsName: "))
    userNsName=os.environ['userNsName']

loginUsername=os.environ.get('loginUsername')
if loginUsername==None:
    os.environ['loginUsername']=str(input("loginUsername: "))
    loginUsername=os.environ['loginUsername']

loginPassword=os.environ.get('loginPassword')
if loginPassword==None:
    os.environ['loginPassword']=str(input("loginPassword: "))
    loginPassword=os.environ['loginPassword']
	
botShouldWait=os.environ.get('botShouldWait') #Wait 1 minute between each action. 0 or 1
if botShouldWait==None:
    os.environ['botShouldWait']=str(input("botShouldWait: "))
    botShouldWait=os.environ['botShouldWait']
	
userAgent=os.environ.get('userAgent')
if userAgent==None:
    os.environ['userAgent']=str(input("userAgent: "))
    userAgent=os.environ['userAgent']

# Get shell arguments
author=sys.argv[1]
targetMentor=sys.argv[2]

# Script variables
inactivityDaysMentors=4 #Max N of inactivity days a mentor can have to be selected as replacement
subject="Il tutor "+targetMentor+" si è ritirato dall'incarico o non è più attivo. Riassegnazione automatica di un nuovo tutor. (Richiesta inoltrata da "+author+" su tutor.toolforge.org)" #Reason to display

# Setup API requests
S = requests.Session()
URL = "https://"+site+".wikipedia.org/w/api.php"
headers = {'User-Agent': userAgent}

#Check if a mentor has been active in the past inactivityDays days
def isActive(mentor, inactivityDays):
    PARAMS={
        "action": "query",
        "format": "json",
        "list": "usercontribs",
        "ucend": datetime.strftime(datetime.today()-timedelta(inactivityDays), '%Y-%m-%d')+"T00:00:00.000Z",
        "ucuser": mentor
    }
    edits=S.get(url=URL, params=PARAMS, headers=headers).json()['query']['usercontribs']

    PARAMS={
        "action": "query",
        "format": "json",
        "list": "logevents",
        "leend": datetime.strftime(datetime.today()-timedelta(inactivityDays), '%Y-%m-%d')+"T00:00:00.000Z",
        "leuser": mentor,
        "lelimit": "1"
    }
    hasLogs=len(S.get(url=URL, params=PARAMS).json()['query']['logevents']) #returns 0 or 1

    return len(edits)>0 or hasLogs == 1

#Get mentors list
mentors=[]
PARAMS = {
	"action": "parse",
	"format": "json",
	"page": mentorsList,
	"prop": "wikitext",
	"formatversion": "2"
}
R=S.get(url=URL, params=PARAMS, headers=headers).json()['parse']['wikitext']
mentors=[]

#Get a list of all the NUMBERS contained between "\"" and "\"" in the wikitext
parsedMentorsIds=R.split("\"")
parsedMentorsIds=parsedMentorsIds[1::2]

#If is not a number, remove from R
for i in range(len(parsedMentorsIds)-1, -1, -1):
    try:
        parsedMentorsIds[i]=int(parsedMentorsIds[i])
    except:
        parsedMentorsIds.pop(i)

for id in parsedMentorsIds:
    #API call to get mentor's usernamee
    PARAMS = {
        "action": "query",
        "format": "json",
        "list": "users",
        "formatversion": "2",
        "ususerids": id
    }
    R=S.get(url=URL, params=PARAMS, headers=headers).json()['query']['users'][0]['name']
    mentors+=[R]
        
#Get mentees list
PARAMS_GET={
	"action": "query",
	"format": "json",
	"list": "growthmentormentee",
	"gemmmentor": targetMentor,
}
menteesList=S.get(url=URL, params=PARAMS_GET, headers=headers).json()['growthmentormentee']['mentees']
print(menteesList) #log
if menteesList==[]:
	raise Exception("The user does not have mentees to reassign.")

#Login
mentor=mentors[randint(0, len(mentors)-1)]
PARAMS_0 = {
	"action": "query",
	"meta": "tokens",
	"type": "login",
	"format": "json"
}
R = S.get(url=URL, params=PARAMS_0, headers=headers)
DATA = R.json()

LOGIN_TOKEN = DATA['query']['tokens']['logintoken']

PARAMS_1 = {
	"action": "login",
        "lgname": loginUsername,
        "lgpassword": loginPassword,
        "lgtoken": LOGIN_TOKEN,
	"format": "json"
}

R = S.post(URL, data=PARAMS_1, headers=headers)

PARAMS_2 = {
	"action": "query",
	"meta": "tokens",
	"format": "json"
}

R = S.get(url=URL, params=PARAMS_2, headers=headers)
DATA = R.json()

CSRF_TOKEN = DATA['query']['tokens']['csrftoken']
	
#Reassign mentor
for mentee in menteesList:
	stop = False
	while (not stop):
		PARAMS_SET={
			"action": "growthsetmentor",
			"format": "json",
			"mentee": mentee["name"],
			"mentor": mentor,
			"reason": subject,
			"token": CSRF_TOKEN
		}
		R=S.post(URL, data=PARAMS_SET, headers={'Content-Type': 'application/x-www-form-urlencoded', 'User-Agent': userAgent})
		print(R.json()) #log
		if list(R.json().keys())[0]=="error":
			if R.json()["error"]["code"]=="maxlag":
				time.sleep(5) #wait, too much lag
				continue
		stop=True

	if int(botShouldWait):
       		time.sleep(60)
